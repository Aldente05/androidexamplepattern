package com.fputra.latihan.helper;

import android.util.Log;

import de.tavendo.autobahn.WebSocketConnection;
import de.tavendo.autobahn.WebSocketException;
import de.tavendo.autobahn.WebSocketHandler;

/**
 * Created by f.putra on 9/20/16.
 */
public class WebSocketHelper {

    private static final String TAG = "de.fputra.test1";

    private final WebSocketConnection mConnection = new WebSocketConnection();

    public void start() {

        final String wsuri = "";

        try {
            mConnection.connect(wsuri, new WebSocketHandler() {

                @Override
                public void onOpen() {
                    Log.d(TAG, "Status: Connected to " + wsuri);
                    mConnection.sendTextMessage("Hello, world!");
                }

                @Override
                public void onTextMessage(String payload) {
                    mConnection.sendTextMessage("Hello, world!");
                    Log.d(TAG, "Got echo: " + payload);
                }

                @Override
                public void onClose(int code, String reason) {
                    Log.d(TAG, "Connection lost.");
                }
            });
        } catch (WebSocketException e) {

            Log.d(TAG, e.toString());
        }
    }

    public String SendData (String param){

        return param;
    }
}
